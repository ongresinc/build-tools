# Build tools 
Build tools project to help in the building of your projects, with all necessary plugins and basic dependencies. 

## Used plugins

- maven-compiler-plugin
- maven-surefire-plugin
- maven-failsafe-plugin
- maven-clean-plugin
- maven-resources-plugin
- maven-dependency-plugin
- maven-jar-plugin
- maven-install-plugin
- maven-enforcer-plugin
- maven-checkstyle-plugin
  - com.puppycrawl.tools / checkstyle
- maven-javadoc-plugin
- maven-shade-plugin
- org.codehaus.mojo / build-helper-maven-plugin
- org.codehaus.mojo / license-maven-plugin
- org.eclipse.m2e / lifecycle-mapping
    
## Profiles    
       
- safer: exec com.github.spotbugs/spotbugs plugin
- integration: exec maven-failsafe-plugin that run all the integration tests (Classes with `It` in the name)

## How to use it
You only have to include this dependency in your parent pom: 
```
  <parent>
    <groupId>com.ongres</groupId>
    <artifactId>build-parent</artifactId>
    <version>1.0.9</version>
  </parent>
``` 
